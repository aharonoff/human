var colors = {
  white: [255, 255, 255],
  gray: [128, 128, 128],
  black: [0, 0, 0],
  blue: [74, 144, 226],
  hover: [128, 177, 235]
}

var images = {
  intro: null,
  outro: null,
  description: null,
  background: [],
  images: [
    [],
    [],
    [],
    [],
    [],
    [],
  ],
  matching: [
    [1, 0, 0, 0, 0, 0, 0, 0, 0],
    [1, 1, 1, 1, 0, 0, 0, 0, 0],
    [1, 0, 0, 0, 0, 0, 0, 0, 0],
    [1, 1, 0, 0, 0, 0, 0, 0, 0],
    [1, 1, 1, 1, 0, 0, 0, 0, 0],
  ],
  selected: create2DArray(3, 3, 0, true)
}

var states = {
  intro: true,
  play: false,
  level: 0,
  counter: 0,
  passed: false,
  outro: false,
  description: false
}

var texts = {
  font: [],
  size: 24,
  description: 'Millions of CAPTCHAs are solved by people every day. reCAPTCHA makes use of this human effort by channeling the time spent solving CAPTCHAs into digitizing text, annotating images, and building machine learning datasets. This in turn helps preserve books, improve maps, and solve hard AI problems. But how big a database needs to be to train a deep learning algorithm to answer question about movies?'
}

function preload() {
  texts.font.push(loadFont('ttf/roboto.ttf'))
  texts.font.push(loadFont('ttf/roboto_bold.ttf'))

  images.intro = loadImage('img/intro.jpg')
  images.outro = loadImage('img/outro.jpg')
  images.description = loadImage('img/description.jpg')

  images.background.push(loadImage('img/alien/bg.jpg'))
  images.background.push(loadImage('img/blade/bg.jpg'))
  images.background.push(loadImage('img/austin/bg.jpg'))
  images.background.push(loadImage('img/matrix/bg.jpg'))
  images.background.push(loadImage('img/exmach/bg.jpg'))

  for (var i = 0; i < 9; i++) {
    images.images[0].push(loadImage('img/alien/' + (i + 1) + '.jpg'))
  }
  for (var i = 0; i < 9; i++) {
    images.images[1].push(loadImage('img/blade/' + (i + 1) + '.jpg'))
  }
  for (var i = 0; i < 9; i++) {
    images.images[2].push(loadImage('img/austin/' + (i + 1) + '.jpg'))
  }
  for (var i = 0; i < 9; i++) {
    images.images[3].push(loadImage('img/matrix/' + (i + 1) + '.jpg'))
  }
  for (var i = 0; i < 9; i++) {
    images.images[4].push(loadImage('img/exmach/' + (i + 1) + '.jpg'))
  }
}

function setup() {
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  background(colors.white)
  textAlign(LEFT, BASELINE)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  if (states.intro === true) {
    image(images.intro, 0, 0, windowWidth, windowHeight)
  }
  if (states.play === true) {
    image(images.background[states.level], 0, 0, windowWidth, windowHeight)
  }
  if (states.outro === true) {
    image(images.outro, 0, 0, windowWidth, windowHeight)
  }
  if (states.description === true) {
    image(images.description, 0, 0, windowWidth, windowHeight)
  }

  if (states.intro === true) {
    fill(colors.white)
    noStroke()
    rect(windowWidth * 0.5, windowHeight * 0.5, 340, 500)

    fill(colors.blue)
    noStroke()
    rect(windowWidth * 0.5, windowHeight * 0.5, 320, 480)

    textFont(texts.font[1])
    textSize(texts.size)
    textAlign(CENTER, BASELINE)
    fill(colors.white)
    noStroke()
    text('Are you a human being?', windowWidth * 0.5, windowHeight * 0.5 + texts.size * 0.5)
  }

  if (states.play === true) {
    fill(colors.white)
    noStroke()
    rect(windowWidth * 0.5, windowHeight * 0.5, 340, 500)

    fill(colors.blue)
    noStroke()
    rect(windowWidth * 0.5, windowHeight * 0.5 - 200, 320, 80)

    fill(colors.white)
    noStroke()
    textFont(texts.font[0])
    textSize(texts.size * 0.75)
    text('Select all images with', windowWidth * 0.5 - 150, windowHeight * 0.5 - 210)
    textFont(texts.font[1])
    textSize(texts.size)
    text('AI (artificial intelligence)', windowWidth * 0.5 - 150, windowHeight * 0.5 - 175)

    for (var i = 0; i < 3; i++) {
      for (var j = 0; j < 3; j++) {
        if (images.selected[i][j] === 0) {
          image(images.images[states.level][i * 3 + j], windowWidth * 0.5 + 110 * (i - 1) - 50, windowHeight * 0.5 + 110 * (j - 1) - 40)
        } else {
          image(images.images[states.level][i * 3 + j], windowWidth * 0.5 + 110 * (i - 1) - 50 + 10, windowHeight * 0.5 + 110 * (j - 1) - 40 + 10, 80, 80)
        }
      }
    }

    if (mouseX >= windowWidth * 0.5 - 80 && mouseX <= windowWidth * 0.5 + 80 && mouseY >= windowHeight * 0.5 + 210 - 20 && mouseY <= windowHeight * 0.5 + 210 + 20) {
      fill(colors.hover)
    } else {
      fill(colors.blue)
    }
    noStroke()
    rect(windowWidth * 0.5, windowHeight * 0.5 + 210, 160, 40)

    textFont(texts.font[0])
    textSize(texts.size)
    textAlign(CENTER, BASELINE)
    fill(colors.white)
    noStroke()
    text('VERIFY', windowWidth * 0.5, windowHeight * 0.5 + 218)

    noFill()
    stroke(colors.blue)
    strokeWeight(6)
    for (var i = 0; i < 3; i++) {
      for (var j = 0; j < 3; j++) {
        if (mouseX >= windowWidth * 0.5 - 55 + (i - 1) * 110 && mouseX <= windowWidth * 0.5 + 55 + (i - 1) * 110 && mouseY >= windowHeight * 0.5 + 10 - 55 + (j - 1) * 110 && mouseY <= windowHeight * 0.5 + 10 + 55 + (j - 1) * 110) {
          rect(windowWidth * 0.5 + (i - 1) * 110, windowHeight * 0.5 + 10 + (j - 1) * 110, 95, 95)
        }
      }
    }
  }

  if (states.outro === true) {
    fill(colors.white)
    noStroke()
    rect(windowWidth * 0.5, windowHeight * 0.5, 340, 500)

    fill(colors.blue)
    noStroke()
    rect(windowWidth * 0.5, windowHeight * 0.5, 320, 480)

    textFont(texts.font[1])
    textSize(texts.size * 1.25)
    textAlign(CENTER, BASELINE)
    fill(colors.white)
    noStroke()
    if (states.passed === true) {
      text('Access confirmed.', windowWidth * 0.5, windowHeight * 0.5 + texts.size * 0.5)
    } else {
      text('Access denied.', windowWidth * 0.5, windowHeight * 0.5 + texts.size * 0.5)
    }
  }

  if (states.description === true) {
    fill(colors.white)
    noStroke()
    rect(windowWidth * 0.5, windowHeight * 0.5, 340, 500)

    fill(colors.blue)
    noStroke()
    rect(windowWidth * 0.5, windowHeight * 0.5, 320, 480)

    textFont(texts.font[1])
    textSize(texts.size * 0.95)
    textAlign(LEFT, BASELINE)
    fill(colors.white)
    noStroke()
    text(texts.description, windowWidth * 0.5, windowHeight * 0.5 + texts.size * 0.5, 300, 480)
  }
}

function mousePressed() {
  if (states.intro === true) {
    setTimeout(function() {
      states.intro = false
    }, 100)
    for (var i = 0; i < 6; i++) {
      shuffleArray(images.images[i], images.matching[i])
    }
    setTimeout(function() {
      states.play = true
    }, 101)
  }
  if (states.play === true) {
    for (var i = 0; i < 3; i++) {
      for (var j = 0; j < 3; j++) {
        if (mouseX >= windowWidth * 0.5 - 55 + (i - 1) * 110 && mouseX <= windowWidth * 0.5 + 55 + (i - 1) * 110 && mouseY >= windowHeight * 0.5 + 10 - 55 + (j - 1) * 110 && mouseY <= windowHeight * 0.5 + 10 + 55 + (j - 1) * 110) {
          if (images.selected[i][j] === 0) {
            images.selected[i][j] = 1
          } else {
            images.selected[i][j] = 0
          }
        }
      }
    }
    if (mouseX >= windowWidth * 0.5 - 80 && mouseX <= windowWidth * 0.5 + 80 && mouseY >= windowHeight * 0.5 + 210 - 20 && mouseY <= windowHeight * 0.5 + 210 + 20) {
      if (arraysEqual(images.matching[states.level], images.selected.flat()) === true) {
        states.counter++
      }
      if (states.level <= 3) {
        setTimeout(function() {
          states.level++
        }, 100)
      } else {
        setTimeout(function() {
          states.play = false
        }, 100)
        setTimeout(function() {
          states.outro = true
        }, 101)
        if (states.counter === 5) {
          states.passed = true
        }
      }
      images.selected = create2DArray(3, 3, 0, true)
    }
  }
  if (states.outro === true) {
    setTimeout(function() {
      states.outro = false
    }, 100)
    setTimeout(function() {
      states.description = true
      states.counter = 0
      states.level = 0
      states.passed = false
    }, 101)
  }
  if (states.description === true) {
    setTimeout(function() {
      states.description = false
    }, 100)
    setTimeout(function() {
      states.intro = true
    }, 101)
  }
}

function arraysEqual(array1, array2) {
  if (array1.length !== array2.length) {
    return false
  }
  for (var i = array1.length; i--;) {
    if (array1[i] !== array2[i]) {
      return false
    }
  }
  return true
}

function shuffleArray(array1, array2) {
  var index = array1.length;
  var rnd, tmp1, tmp2;

  while (index) {
    rnd = Math.floor(Math.random() * index);
    index -= 1;
    tmp1 = array1[index];
    tmp2 = array2[index];
    array1[index] = array1[rnd];
    array2[index] = array2[rnd];
    array1[rnd] = tmp1;
    array2[rnd] = tmp2;
  }
  return array1, array2
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}

function windowResized() {
  createCanvas(windowWidth, windowHeight)
}
